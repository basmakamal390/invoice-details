<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$products = array(
    array(
        "item_type" => "T-shirt",
        "country" => "US",
        "price" => "$30.99",
        "weight" => "0.2",
        "rate" => "$2",
        "shipping" => "$4",
        "vat" => "$4.3386",
    ) ,
    array(
        "item_type" => "Blouse",
        "country" => "UK",
        "price" => "$10.99",
        "weight" => "0.3",
        "rate" => "$3",
        "shipping" => "$9",
        "vat" => "$1.5386",
    ) ,
    array(
        "item_type" => "Pants",
        "country" => "UK",
        "price" => "$64.99",
        "weight" => "0.9",
        "rate" => "$3",
        "shipping" => "$27",
        "vat" => "$9.0986",
    ) ,
    array(
        "item_type" => "Sweatpants",
        "country" => "CN",
        "price" => "$84.99",
        "weight" => "1.1",
        "rate" => "$2",
        "shipping" => "$22",
        "vat" => "$11.8986",
    ) ,
    array(
        "item_type" => "Jacket",
        "country" => "US",
        "price" => "$199.99",
        "weight" => "2.2",
        "rate" => "$2",
        "shipping" => "$44",
        "vat" => "$27.9986",
    ) ,
    array(
        "item_type" => "Shoes",
        "country" => "CN",
        "price" => "$79.99",
        "weight" => "1.3",
        "rate" => "$2",
        "shipping" => "$26",
        "vat" => "$11.1986",
    ) ,

);
// get posted data
$data = json_decode(file_get_contents("php://input"));
// checkif the data sent on array format
if (is_array($data->products))
{
	// iniliaze empty price and discout values
    $subtotal = 0;
    $totalShipping = 0;
    $totalVat = 0;
    $totalDiscout = 0;
    $discountDesc = " ";
	//search in array having tow of tishirt or Blouse to apply second discount on jackey
    $totalProductDiscouted = count(array_keys($data->products, 'T-shirt')) + count(array_keys($data->products, 'Blouse'));

    foreach ($data->products as $value)
    {

        $keyOData = array_search($value, array_column($products, 'item_type'));

        $subtotal += (float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['price']);
        $totalShipping += (float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['shipping']);
        $totalVat += (float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['vat']);
        //first Offer Of shoes
        if ($value == 'Shoes')
        {
            $totalDiscout += ((float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['price']) / 100) * 10;
            $discountDesc .= "<br> 10% off shoes: $" . ((float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['price']) / 100) * 10;
        }
        //second Offer Of Jacket
        if ($totalProductDiscouted >= 2 && $value == 'Jacket')
        {
            $totalDiscout += ((float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['price']) / 100) * 50;
            $discountDesc .= "<br> 50% off jacket: -$" . ((float)preg_replace('/[^0-9\.]/ui', '', $products[$keyOData]['price']) / 100) * 50;
        }
    }
    // third  offer of more than or equal tow items shipping
    if (count($data->products) >= 2)
    {
        $totalDiscout += ($totalShipping / 100) * 10;
        $discountDesc .= "<br> $10 of shipping: -$" . ($totalShipping / 100) * 10;

    }

    $total = $subtotal + $totalShipping + $totalVat - $totalDiscout;
    // final result
    echo json_encode(array(
        "Subtotal" => "$." . $subtotal,
        "Shipping" => "$." . $totalShipping,
        "VAT" => "$." . $totalVat,
        "Discounts" => $discountDesc,
        "Total" => "$" . $total,

    ));

}
else
{
    echo "error sending data array format";
}

